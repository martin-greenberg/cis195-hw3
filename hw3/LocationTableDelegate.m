//
//  LocationTableDelegate.m
//  hw3
//
//  Copyright (c) 2014 Martin Greenberg. All rights reserved.
//


#import "FlickrFetcher.h"
#import "LocationTableDelegate.h"
#import "PhotosTableDelegate.h"
#import "LocationTableDetailViewController.h"

@interface LocationTableDelegate ()

@end

@implementation LocationTableDelegate
{
	NSArray *tableData;
}

- (void)viewDidLoad {
	[super viewDidLoad];
	tableData = [FlickrFetcher topPlaces];
	self.refreshControl = [[UIRefreshControl alloc] init];
	[self.refreshControl addTarget:self
							action:@selector(getNewLocations)
				  forControlEvents:UIControlEventValueChanged];
	
}
- (void)getNewLocations
{
	dispatch_queue_t updateQueue = dispatch_queue_create("locations_queue", NULL);
	dispatch_async(updateQueue, ^{
		[self.tableView reloadData];
		
		// End the refreshing
		if (self.refreshControl) {
			[self.refreshControl endRefreshing];
		}
	});
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return (NSInteger)[tableData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *simpleTableIdentifier = @"Location cell";
	
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
	
	if (cell == nil) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
	}
	
	cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
	cell.textLabel.text = [[tableData objectAtIndex:indexPath.row] objectForKey:@"woe_name"];
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[self performSegueWithIdentifier: @"showPhotosForLocation" sender: [tableData objectAtIndex:indexPath.row]];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if([[segue identifier] isEqualToString:@"showPhotosForLocation"])
	{
		UINavigationController *target = [segue destinationViewController];
		PhotosTableDelegate* photosView = target.childViewControllers[0];
		photosView.sendingObject = sender;
	}
	if([[segue identifier] isEqualToString:@"showLocationDetail"])
	{
		UINavigationController *target = [segue destinationViewController];
		LocationTableDetailViewController* detailView = target.childViewControllers[0];
		detailView.photoObjects = [FlickrFetcher photosInPlace:sender maxResults:100];
	}
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
	[self performSegueWithIdentifier:@"showLocationDetail" sender:[tableData objectAtIndex:indexPath.row]];
}

@end
