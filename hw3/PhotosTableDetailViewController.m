//
//  PhotosTableDetailViewController.h
//  hw3
//
//  Created by Martin Greenberg on 10/10/14.
//  Copyright (c) 2014 Martin Greenberg. All rights reserved.
//

#import "PhotosTableDetailViewController.h"
#import "FlickrFetcher.h"
@import MapKit;

@interface PhotosTableDetailViewController ()
@property (strong, nonatomic) IBOutlet UILabel *photoTitle;
@property (strong, nonatomic) IBOutlet UILabel *ownerName;
@property (strong, nonatomic) IBOutlet MKMapView *photoLocation;
@property (strong, nonatomic) IBOutlet UIImageView *photoPreview;
@end

@implementation PhotosTableDetailViewController

- (void)viewDidLoad {
	[super viewDidLoad];
	if(self.photoObject == nil){
		NSLog(@"No photo object was passed to detail view.");
		return;
	} else {
		[self.photoTitle setText:[self.photoObject objectForKey:@"title"]];
		[self.ownerName setText:[self.photoObject objectForKey:@"ownername"]];
		double photoLatitude = [[self.photoObject objectForKey:@"latitude"] doubleValue];
		double photoLongitude = [[self.photoObject objectForKey:@"longitude"] doubleValue];
		CLLocationCoordinate2D photoLocationCoord = CLLocationCoordinate2DMake(photoLatitude, photoLongitude);
		self.photoLocation.region = MKCoordinateRegionMakeWithDistance(photoLocationCoord, 1000., 1000.);
		MKPointAnnotation *pin = [[MKPointAnnotation alloc] init];
		[pin setCoordinate:photoLocationCoord];
		pin.title = self.photoTitle.text;
		pin.subtitle = self.ownerName.text;
		[self.photoLocation addAnnotation:pin];
		NSURL* imageURL = [FlickrFetcher urlForPhoto:self.photoObject format:FlickrPhotoFormatLarge];
		UIImage* previewedImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:imageURL]];
		if((CGImageGetWidth(previewedImage.CGImage) < _photoPreview.bounds.size.width)||
		   (CGImageGetHeight(previewedImage.CGImage) < _photoPreview.bounds.size.height)){
			int widthMargin = (_photoPreview.bounds.size.width - CGImageGetWidth(previewedImage.CGImage))/2;
			int heightMargin = (_photoPreview.bounds.size.width - CGImageGetWidth(previewedImage.CGImage))/2;
			_photoPreview.bounds = CGRectMake(_photoPreview.bounds.origin.x+widthMargin, _photoPreview.bounds.origin.y+heightMargin, CGImageGetWidth(previewedImage.CGImage), CGImageGetHeight(previewedImage.CGImage));
		}
		[self.photoPreview setImage:previewedImage];
	}
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

@end
