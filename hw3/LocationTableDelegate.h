
//
//  LocationTableDelegate.h
//  hw3
//
//  Copyright (c) 2014 Martin Greenberg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LocationTableDelegate : UITableViewController <UITableViewDelegate, UITableViewDataSource>

@end
