//
//  PhotosTableDelegate.m
//  hw3
//
//  Copyright (c) 2014 Martin Greenberg. All rights reserved.
//


#import "FlickrFetcher.h"
#import "PhotosTableDelegate.h"
#import "PhotoScrollViewController.h"
#import "PhotosTableDetailViewController.h"

@interface PhotosTableDelegate ()

@end

@implementation PhotosTableDelegate
{
	NSArray *tableData;
}

- (void)viewDidLoad {
	[super viewDidLoad];
	self.title = [NSString stringWithFormat:@"Photos from %@",[self.sendingObject objectForKey:@"woe_name"]];
	if(self.sendingObject == nil) NSLog(@"No location selection was sent");
	tableData = [FlickrFetcher photosInPlace:self.sendingObject maxResults:100];
	self.refreshControl = [[UIRefreshControl alloc] init];
	[self.refreshControl addTarget:self
							action:@selector(getNewPhotosForLocation)
				  forControlEvents:UIControlEventValueChanged];

}
- (void)getNewPhotosForLocation
{
	dispatch_queue_t updateQueue = dispatch_queue_create("photos_queue", NULL);
	dispatch_async(updateQueue, ^{
		[self.tableView reloadData];
		
		if (self.refreshControl) {
			[self.refreshControl endRefreshing];
		}
	});
}
- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return (NSInteger)[tableData count];
}

+ (UIImage*) compressImageForThumbnail:(UIImage*)originalImage
{
	CGSize originalSize = originalImage.size;
	CGSize newSize = CGSizeMake(100, 100);
	
	UIGraphicsBeginImageContext(newSize);
	[originalImage drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
	UIImage* compressedImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	return compressedImage;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *simpleTableIdentifier = @"Photo cell";
	
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
	
	if (cell == nil) {
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:simpleTableIdentifier];
	}
	dispatch_queue_t thumbnailQueue = dispatch_queue_create("thumbnail_queue", NULL);
	dispatch_async(thumbnailQueue, ^{
		NSURL* imageURL = [FlickrFetcher urlForPhoto:[tableData objectAtIndex:indexPath.row] format:FlickrPhotoFormatLarge];
		UIImage* unscaled = [UIImage imageWithData:[NSData dataWithContentsOfURL:imageURL]];
		[cell.imageView setImage:[PhotosTableDelegate compressImageForThumbnail:unscaled]];
	});
	cell.textLabel.text = [[tableData objectAtIndex:indexPath.row] objectForKey:@"title"];
	cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
	cell.detailTextLabel.text = [[tableData objectAtIndex:indexPath.row] objectForKey:@"ownername"];
	return cell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[self performSegueWithIdentifier: @"showImage" sender: [tableData objectAtIndex:indexPath.row]];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if([[segue identifier] isEqualToString:@"showImage"])
	{
		UINavigationController *target = [segue destinationViewController];
		PhotoScrollViewController* photoScrollView = target.childViewControllers[0];
		photoScrollView.imageURL = [FlickrFetcher urlForPhoto:sender format:FlickrPhotoFormatLarge];
	}
	if([[segue identifier] isEqualToString:@"showPhotoDetail"])
	{
		UINavigationController *target = [segue destinationViewController];
		PhotosTableDetailViewController* photoDetailView = target.childViewControllers[0];
		photoDetailView.photoObject = sender;
		NSLog(@"");
	}
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
	[self performSegueWithIdentifier:@"showPhotoDetail" sender:[tableData objectAtIndex:indexPath.row]];
}
@end
