//
//  ViewController.h
//  ScrollVIewDemo
//
//  Created by William McDermid on 9/24/14.
//  Copyright (c) 2014 William McDermid. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotoScrollViewController : UIViewController <UIScrollViewDelegate>

@property (strong, nonatomic) NSURL* imageURL;
@end

