//
//  ViewController.m
//  ScrollVIewDemo
//
//  Created by William McDermid on 9/24/14.
//  Copyright (c) 2014 William McDermid. All rights reserved.
//

#import "PhotoScrollViewController.h"

@interface PhotoScrollViewController ()
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

@end

@implementation PhotoScrollViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    // Create our image
	if(self.imageURL == nil){
		NSLog(@"No image URL found.");
		return;
	}
	UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:self.imageURL]];
	// Create a UIImageView containing our image
    UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
    // Add the UIImageView to the UIScrollView
    [self.scrollView addSubview:imageView];
    // Resize the UIScrollView to fit our UIImageView
    self.scrollView.contentSize = imageView.bounds.size;
    
    // Set the delegate of the UIScrollView to be this view controller
    // Combined with the function viewForZoomingInScrollView, this allows
    //      us to zoom our image.
    // The compiler will mark this line with a warning unless we add the
    //      UIScrollViewDelegate protocol to our .h file, but zooming will
    //      still work because of "respondsToSelector"
    self.scrollView.delegate = self;
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)sender
{
    // Return the subview of UIScrollView that we want to scale when zooming
    return [sender.subviews objectAtIndex:0];
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView
{
    // Obtain a reference to our UIImageView
    UIView *subView = [scrollView.subviews objectAtIndex:0];
    
    // Compute the offset of the UIImageView within the UIScrollView
    CGFloat offsetX = MAX((scrollView.bounds.size.width - scrollView.contentSize.width) * 0.5, 0.0);
    CGFloat offsetY = MAX((scrollView.bounds.size.height - scrollView.contentSize.height) * 0.5, 0.0);
    
    // Position the UIImageView so it is centered in the UIScrollView
    subView.center = CGPointMake(scrollView.contentSize.width * 0.5 + offsetX,
                                 scrollView.contentSize.height * 0.5 + offsetY);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
