//
//  PhotosTableDetailViewController.h
//  hw3
//
//  Created by Martin Greenberg on 10/10/14.
//  Copyright (c) 2014 Martin Greenberg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotosTableDetailViewController : UIViewController

@property (strong, nonatomic) NSDictionary* photoObject;
@end

