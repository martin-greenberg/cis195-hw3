
//
//  PhotosTableDelegate.h
//  hw3
//
//  Copyright (c) 2014 Martin Greenberg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotosTableDelegate : UITableViewController <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) NSDictionary* sendingObject;
@end
