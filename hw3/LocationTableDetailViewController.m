//
//  LocationTableDetailViewController.m
//  hw3
//
//  Created by Martin Greenberg on 10/10/14.
//  Copyright (c) 2014 Martin Greenberg. All rights reserved.
//

#import "LocationTableDetailViewController.h"
#import "FlickrFetcher.h"
@import MapKit;

@interface LocationTableDetailViewController ()
@property (strong, nonatomic) IBOutlet MKMapView *photoLocations;
@end

@implementation LocationTableDetailViewController

- (void)viewDidLoad {
	[super viewDidLoad];
	if(self.photoObjects == nil){
		NSLog(@"No photo object was passed to detail view.");
		return;
	} else {
		for(NSDictionary* photo in self.photoObjects){
			double photoLatitude = [[photo objectForKey:@"latitude"] doubleValue];
			double photoLongitude = [[photo objectForKey:@"longitude"] doubleValue];
			CLLocationCoordinate2D photoLocationCoord = CLLocationCoordinate2DMake(photoLatitude, photoLongitude);
			MKPointAnnotation *pin = [[MKPointAnnotation alloc] init];
			[pin setCoordinate:photoLocationCoord];
			pin.title = [photo objectForKey:@"title"];
			pin.subtitle = [photo objectForKey:@"ownername"];
			[self.photoLocations addAnnotation:pin];
		}
	}
	double latsum = 0;
	double longsum = 0;
	double maxlat = 0;
	double minlat = 0;
	double maxlon = 0;
	double minlon = 0;
	for(id<MKAnnotation> annotationObj in self.photoLocations.annotations){
		latsum += annotationObj.coordinate.latitude;
		longsum += annotationObj.coordinate.longitude;
		if(annotationObj.coordinate.latitude > maxlat) maxlat = annotationObj.coordinate.latitude;
		if(annotationObj.coordinate.longitude > maxlon) maxlon = annotationObj.coordinate.longitude;
		if(annotationObj.coordinate.latitude < minlat) minlat = annotationObj.coordinate.latitude;
		if(annotationObj.coordinate.longitude < maxlon) minlon = annotationObj.coordinate.longitude;
	}
	CLLocationCoordinate2D avgCoord = CLLocationCoordinate2DMake(latsum/100, longsum/100);
	self.photoLocations.region = MKCoordinateRegionMakeWithDistance(avgCoord, 500*abs(maxlat-minlat), 500*abs(maxlon-minlon));
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

@end
