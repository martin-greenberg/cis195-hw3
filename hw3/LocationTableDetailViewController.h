//
//  LocationTableDetailViewController.h
//  hw3
//
//  Created by Martin Greenberg on 10/10/14.
//  Copyright (c) 2014 Martin Greenberg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LocationTableDetailViewController : UIViewController

@property (strong, nonatomic) NSArray* photoObjects;
@end
